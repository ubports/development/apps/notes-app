# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the lomiri-notes-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: lomiri-notes-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-11 09:22+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: lomiri-notes-app.desktop.in:3 src/app/qml/ui/HelpPage.qml:74
msgid "Notes"
msgstr ""

#: lomiri-notes-app.desktop.in:4
msgid "Ubuntu Notes app, powered by Evernote"
msgstr ""

#: lomiri-notes-app.desktop.in:6
msgid "/usr/share/lomiri-notes-app/lomiri-notes-app.png"
msgstr ""

#: src/app/qml/Reminders.qml:130 src/app/qml/ui/EditNotePage.qml:35
#: src/app/qml/ui/EditNotePage.qml:62 src/app/qml/ui/MainPage.qml:140
msgid "Untitled"
msgstr ""

#: src/app/qml/Reminders.qml:379
msgid "Default notebook"
msgstr ""

#: src/app/qml/Reminders.qml:505
msgid "Sync with Evernote"
msgstr ""

#: src/app/qml/Reminders.qml:506
msgid "Notes can be stored on this device, or optionally synced with Evernote."
msgstr ""

#: src/app/qml/Reminders.qml:507
msgid "To sync with Evernote, you need an Evernote account."
msgstr ""

#: src/app/qml/Reminders.qml:522
msgid "Not Now"
msgstr ""

#: src/app/qml/Reminders.qml:531
msgid "Set Up…"
msgstr ""

#: src/app/qml/Reminders.qml:547
#, qt-format
msgid "Importing %1 items"
msgstr ""

#: src/app/qml/Reminders.qml:548
msgid "Importing 1 item"
msgstr ""

#: src/app/qml/Reminders.qml:550
msgid ""
"Do you want to create a new note for those items or do you want to attach "
"them to an existing note?"
msgstr ""

#: src/app/qml/Reminders.qml:551
msgid ""
"Do you want to create a new note for this item or do you want to attach it "
"to an existing note?"
msgstr ""

#: src/app/qml/Reminders.qml:557
msgid "Create new note"
msgstr ""

#: src/app/qml/Reminders.qml:563
msgid "Attach to existing note"
msgstr ""

#: src/app/qml/Reminders.qml:567
msgid "Cancel import"
msgstr ""

#: src/app/qml/components/DeleteConfirmationDialog.qml:31
#: src/app/qml/components/DeleteConfirmationDialog.qml:37
#: src/app/qml/components/NotebookDelegate.qml:48
#: src/app/qml/components/ReminderDelegate.qml:42
#: src/app/qml/components/TagDelegate.qml:38
msgid "Delete"
msgstr ""

#. TRANSLATORS: %1 is translatable, it is one of the following: note, notebook, tag, reminder.
#. %2 is untranslatable, it is the name of the note, notebook, tag or reminder.
#: src/app/qml/components/DeleteConfirmationDialog.qml:34
#, qt-format
msgid "Are you sure you want to delete %1 <b>%2</b>?"
msgstr ""

#: src/app/qml/components/DeleteConfirmationDialog.qml:47
#: src/app/qml/components/RenameDialog.qml:52
#: src/app/qml/components/SelectNotebookDialog.qml:66
#: src/app/qml/components/SelectTagsDialog.qml:85
msgid "Cancel"
msgstr ""

#: src/app/qml/components/NoteDelegate.qml:66
#: src/app/qml/ui/EditNotePage.qml:80 src/app/qml/ui/NotePage.qml:58
msgid "Filters"
msgstr ""

#: src/app/qml/components/NoteDelegate.qml:75
#: src/app/qml/components/ReminderDelegate.qml:55
#: src/app/qml/components/TagDelegate.qml:51 src/app/qml/ui/NotePage.qml:49
msgid "Edit"
msgstr ""

#: src/app/qml/components/NoteDetailsDialog.qml:25
#: src/app/qml/ui/EditNoteFiltersPage.qml:104
msgid "Details"
msgstr ""

#: src/app/qml/components/NoteDetailsDialog.qml:34
msgid "Created"
msgstr ""

#: src/app/qml/components/NoteDetailsDialog.qml:45
msgid "Updated"
msgstr ""

#: src/app/qml/components/NoteDetailsDialog.qml:51
msgid "Close"
msgstr ""

#: src/app/qml/components/NotebookDelegate.qml:62
msgid "Set as default"
msgstr ""

#: src/app/qml/components/NotebookDelegate.qml:69
#: src/app/qml/components/RenameDialog.qml:28
msgid "Rename"
msgstr ""

#: src/app/qml/components/PulldownListView.qml:41
msgid "Release to refresh"
msgstr ""

#: src/app/qml/components/PulldownListView.qml:41
msgid "Pull down to refresh"
msgstr ""

#: src/app/qml/components/RenameDialog.qml:28
msgid "Create"
msgstr ""

#: src/app/qml/components/RenameDialog.qml:37
msgid "Name cannot be empty"
msgstr ""

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:26
msgid "Resolve conflict"
msgstr ""

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:31
msgid ""
"This will <b>keep the changes made on this device</b> and <b>restore the "
"note on Evernote</b>."
msgstr ""

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:33
msgid "This will <b>delete the changed note on Evernote</b>."
msgstr ""

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:35
msgid ""
"This will <b>keep the changes made on this device</b> and <b>discard any "
"changes made on Evernote</b>."
msgstr ""

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:39
msgid "This will <b>delete the changed note on this device</b>."
msgstr ""

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:41
msgid ""
"This will <b>download the changed note from Evernote</b> and <b>restore it "
"on this device</b>."
msgstr ""

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:43
msgid ""
"This will <b>download the changed note from Evernote</b> and <b>discard any "
"changes made on this device</b>."
msgstr ""

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:46
msgid "Are you sure you want to continue?"
msgstr ""

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:58
msgid "Yes"
msgstr ""

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:68
msgid "No"
msgstr ""

#: src/app/qml/components/SelectNotebookDialog.qml:27
msgid "Select notebook"
msgstr ""

#: src/app/qml/components/SelectNotebookDialog.qml:56
#: src/app/qml/components/SelectTagsDialog.qml:74
msgid "Select"
msgstr ""

#: src/app/qml/components/SelectTagsDialog.qml:27
msgid "Select tags"
msgstr ""

#: src/app/qml/components/SelectTagsDialog.qml:37
#: src/app/qml/ui/TagsPage.qml:83
msgid "No tags available"
msgstr ""

#: src/app/qml/components/SortingDialog.qml:9
msgid "Sort by"
msgstr ""

#: src/app/qml/components/SortingDialog.qml:19
msgid "Date created (newest first)"
msgstr ""

#: src/app/qml/components/SortingDialog.qml:20
msgid "Date created (oldest first)"
msgstr ""

#: src/app/qml/components/SortingDialog.qml:21
msgid "Date updated (newest first)"
msgstr ""

#: src/app/qml/components/SortingDialog.qml:22
msgid "Date updated (oldest first)"
msgstr ""

#: src/app/qml/components/SortingDialog.qml:23
msgid "Title (ascending)"
msgstr ""

#: src/app/qml/components/SortingDialog.qml:24
msgid "Title (descending)"
msgstr ""

#: src/app/qml/components/ToolBox.qml:48
msgid "Font"
msgstr ""

#: src/app/qml/components/ToolBox.qml:59
msgid "Size"
msgstr ""

#. TRANSLATORS: Toolbar button for "Bold"
#: src/app/qml/components/ToolBox.qml:83
msgid "B"
msgstr ""

#. TRANSLATORS: Toolbar button for "Italic"
#: src/app/qml/components/ToolBox.qml:96
msgid "I"
msgstr ""

#. TRANSLATORS: Toolbar button for "Underline"
#: src/app/qml/components/ToolBox.qml:110
msgid "U"
msgstr ""

#. TRANSLATORS: Toolbar button for "Strikeout"
#: src/app/qml/components/ToolBox.qml:124
msgid "T"
msgstr ""

#: src/app/qml/ui/AccountSelectorPage.qml:36
msgid "Select account"
msgstr ""

#: src/app/qml/ui/AccountSelectorPage.qml:49
msgid "Current account"
msgstr ""

#: src/app/qml/ui/AccountSelectorPage.qml:56
msgid "Store notes locally only"
msgstr ""

#: src/app/qml/ui/AccountSelectorPage.qml:66
msgid "Accounts on www.evernote.com"
msgstr ""

#: src/app/qml/ui/AccountSelectorPage.qml:97
#, qt-format
msgid "%1 - Tap to authorize"
msgstr ""

#: src/app/qml/ui/AccountSelectorPage.qml:106
msgid "Add new account"
msgstr ""

#: src/app/qml/ui/EditNoteFiltersPage.qml:59
msgid "Notebook"
msgstr ""

#: src/app/qml/ui/EditNoteFiltersPage.qml:76 src/app/qml/ui/HelpPage.qml:98
msgid "Tags"
msgstr ""

#: src/app/qml/ui/EditNoteFiltersPage.qml:93
msgid "Reminder"
msgstr ""

#: src/app/qml/ui/EditNotePage.qml:71
msgid "Save and close"
msgstr ""

#: src/app/qml/ui/HelpPage.qml:31 src/app/qml/ui/SettingsPage.qml:35
msgid "Help"
msgstr ""

#: src/app/qml/ui/HelpPage.qml:42
msgid "Report an issue"
msgstr ""

#: src/app/qml/ui/HelpPage.qml:53
msgid "Introducing the new navigation panel"
msgstr ""

#: src/app/qml/ui/HelpPage.qml:54
msgid "Go ahead and select an icon below"
msgstr ""

#: src/app/qml/ui/HelpPage.qml:86
msgid "Notebooks"
msgstr ""

#: src/app/qml/ui/HelpPage.qml:110
msgid "Reminders"
msgstr ""

#: src/app/qml/ui/ImportPage.qml:33
msgid "Import image from"
msgstr ""

#: src/app/qml/ui/MainPage.qml:47
msgid "Add note"
msgstr ""

#: src/app/qml/ui/MainPage.qml:61
msgid "Add notebook"
msgstr ""

#: src/app/qml/ui/MainPage.qml:75
msgid "Add tag"
msgstr ""

#: src/app/qml/ui/MainPage.qml:95 src/app/qml/ui/SettingsPage.qml:32
msgid "Settings"
msgstr ""

#: src/app/qml/ui/MainPage.qml:103
msgid "Search"
msgstr ""

#: src/app/qml/ui/MainPage.qml:143 src/app/qml/ui/NotebooksPage.qml:61
#: src/app/qml/ui/NotebooksPage.qml:106
msgid "notebook"
msgstr ""

#: src/app/qml/ui/MainPage.qml:150 src/app/qml/ui/TagsPage.qml:62
#: src/app/qml/ui/TagsPage.qml:67
msgid "tag"
msgstr ""

#: src/app/qml/ui/NoteConflictPage.qml:33
msgid "Conflicting changes"
msgstr ""

#: src/app/qml/ui/NoteConflictPage.qml:43
msgid ""
"This note has been modified in multiple places. Open each version to check "
"the content, then swipe to keep one of them."
msgstr ""

#: src/app/qml/ui/NoteConflictPage.qml:49
msgid "Deleted locally:"
msgstr ""

#: src/app/qml/ui/NoteConflictPage.qml:49
msgid "Modified locally:"
msgstr ""

#: src/app/qml/ui/NoteConflictPage.qml:69
#: src/app/qml/ui/NoteConflictPage.qml:107
msgid "Keep"
msgstr ""

#: src/app/qml/ui/NoteConflictPage.qml:87
msgid "Deleted on Evernote:"
msgstr ""

#: src/app/qml/ui/NoteConflictPage.qml:87
msgid "Modified on Evernote:"
msgstr ""

#: src/app/qml/ui/NotePage.qml:149
msgid "Attachment"
msgstr ""

#: src/app/qml/ui/NotesPage.qml:62
msgid "note"
msgstr ""

#: src/app/qml/ui/NotesPage.qml:94
msgid "No notes available"
msgstr ""

#: src/app/qml/ui/RemindersPage.qml:58 src/app/qml/ui/SetReminderPage.qml:165
msgid "reminder"
msgstr ""

#: src/app/qml/ui/RemindersPage.qml:78
msgid "No reminders available"
msgstr ""

#: src/app/qml/ui/SearchNotebooksPage.qml:31
msgid "Search notebooks..."
msgstr ""

#: src/app/qml/ui/SearchNotesPage.qml:35
msgid "Search notes..."
msgstr ""

#: src/app/qml/ui/SearchTagsPage.qml:31
msgid "Search tags..."
msgstr ""

#: src/app/qml/ui/SetReminderPage.qml:45
msgid "Set"
msgstr ""

#: src/app/qml/ui/SetReminderPage.qml:97
msgid "Select date"
msgstr ""

#: src/app/qml/ui/SetReminderPage.qml:127
msgid "Select time"
msgstr ""

#: src/app/qml/ui/SetReminderPage.qml:158
msgid "Delete reminder"
msgstr ""

#: src/app/qml/ui/SettingsPage.qml:51
msgid "Account"
msgstr ""

#: src/app/qml/ui/SettingsPage.qml:63
msgid "Notebook colors"
msgstr ""

#: src/app/qml/ui/settings/ThemeSettings.qml:41
msgid "App theme"
msgstr ""

#: src/app/qml/ui/settings/ThemeSettings.qml:44
msgid "System"
msgstr ""

#: src/app/qml/ui/settings/ThemeSettings.qml:45
msgid "Ambiance"
msgstr ""

#: src/app/qml/ui/settings/ThemeSettings.qml:46
msgid "SuruDark"
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:268
msgid "Offline mode"
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:273
#: src/libqtevernote/evernoteconnection.cpp:362
msgid "Unknown error connecting to Evernote."
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:286
msgid ""
"Error connecting to Evernote: Server version does not match app version. "
"Please update the application."
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:292
#: src/libqtevernote/evernoteconnection.cpp:297
#, qt-format
msgid "Error connecting to Evernote: Error code %1"
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:302
msgid ""
"Error connecting to Evernote: Cannot download version information from "
"server."
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:307
msgid "Unknown error connecting to Evernote"
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:321
msgid "Error connecting to Evernote: Cannot download server information."
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:329
#: src/libqtevernote/notesstore.cpp:1790
msgid ""
"Authentication for Evernote server expired. Please renew login information "
"in the accounts settings."
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:332
#: src/libqtevernote/evernoteconnection.cpp:350
#, qt-format
msgid "Unknown error connecting to Evernote: %1"
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:341
msgid ""
"Error connecting to Evernote: Rate limit exceeded. Please try again later."
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:357
msgid ""
"Error connecting to Evernote: Connection failure when downloading server "
"information."
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:383
msgid "Error connecting to Evernote: Connection failure"
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:387
msgid "Unknown Error connecting to Evernote"
msgstr ""

#: src/libqtevernote/evernoteconnection.cpp:435
#: src/libqtevernote/evernoteconnection.cpp:499
msgid "Disconnected from Evernote."
msgstr ""

#: src/libqtevernote/note.cpp:164 src/libqtevernote/note.cpp:198
#: src/libqtevernote/note.cpp:389
msgid "Today"
msgstr ""

#: src/libqtevernote/note.cpp:167 src/libqtevernote/note.cpp:201
msgid "Yesterday"
msgstr ""

#: src/libqtevernote/note.cpp:170 src/libqtevernote/note.cpp:204
msgid "Last week"
msgstr ""

#: src/libqtevernote/note.cpp:173 src/libqtevernote/note.cpp:207
msgid "Two weeks ago"
msgstr ""

#. TRANSLATORS: the first argument refers to a month name and the second to a year
#: src/libqtevernote/note.cpp:177 src/libqtevernote/note.cpp:211
#, qt-format
msgid "%1 %2"
msgstr ""

#: src/libqtevernote/note.cpp:377
msgid "Done"
msgstr ""

#: src/libqtevernote/note.cpp:383
msgid "No date"
msgstr ""

#: src/libqtevernote/note.cpp:386
msgid "Overdue"
msgstr ""

#: src/libqtevernote/note.cpp:392
msgid "Tomorrow"
msgstr ""

#: src/libqtevernote/note.cpp:395
msgid "Next week"
msgstr ""

#: src/libqtevernote/note.cpp:398
msgid "In two weeks"
msgstr ""

#: src/libqtevernote/note.cpp:400
msgid "Later"
msgstr ""

#. TRANSLATORS: this is part of a longer string - "Last updated: today"
#: src/libqtevernote/notebook.cpp:122
msgid "today"
msgstr ""

#. TRANSLATORS: this is part of a longer string - "Last updated: yesterday"
#: src/libqtevernote/notebook.cpp:126
msgid "yesterday"
msgstr ""

#. TRANSLATORS: this is part of a longer string - "Last updated: last week"
#: src/libqtevernote/notebook.cpp:130
msgid "last week"
msgstr ""

#. TRANSLATORS: this is part of a longer string - "Last updated: two weeks ago"
#: src/libqtevernote/notebook.cpp:134
msgid "two weeks ago"
msgstr ""

#. TRANSLATORS: this is used in the notes list to group notes created on the same month
#. the first parameter refers to a month name and the second to a year
#: src/libqtevernote/notebook.cpp:138
#, qt-format
msgid "on %1 %2"
msgstr ""

#: src/libqtevernote/notesstore.cpp:439
msgid ""
"This account is managed by Evernote. Use the Evernote website to delete "
"notebooks."
msgstr ""

#: src/libqtevernote/notesstore.cpp:453
msgid ""
"Cannot delete the default notebook. Set another notebook to be the default "
"first."
msgstr ""

#: src/libqtevernote/notesstore.cpp:1793
msgid "Rate limit for Evernote server exceeded. Please try again later."
msgstr ""

#: src/libqtevernote/notesstore.cpp:1796
msgid "Upload quota for Evernote server exceeded. Please try again later."
msgstr ""

#: src/libqtevernote/notesstore.cpp:1851
msgid ""
"This account is managed by Evernote. Please use the Evernote website to "
"delete tags."
msgstr ""

#. TRANSLATORS: A default file name if we don't get one from the server. Avoid weird characters.
#: src/libqtevernote/resource.cpp:39
msgid "Unnamed"
msgstr ""
