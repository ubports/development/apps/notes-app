/*
 * Copyright: 2013 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Lomiri.Components.Popups 1.3
import Evernote 0.1

import "../components"

Page {
    id: root

    header: PageHeader {
        id: header
        contents: TextField {
            id: searchField
            anchors.fill: parent
            placeholderText: i18n.tr("Search notes...")
            inputMethodHints: Qt.ImhNoPredictiveText

            onTextChanged: {
                NotesStore.findNotes(searchField.text)
            }
        }
    }

    NotesPage {
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        page: root
        onlySearchResults: true
    }

    Timer {
        id: focusDelayTimer
        interval: 100

        onTriggered: {
            searchField.focus = true
            NotesStore.findNotes(searchField.text)
        }
    }

    Component.onCompleted: {
        focusDelayTimer.start()
    }
}
