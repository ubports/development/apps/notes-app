/*
 * Copyright: 2013 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Pickers 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Components.Themes.Ambiance 1.3
import Evernote 0.1

Page {
    id: root

    property var note

    header: PageHeader {
        property string notebookColor: {
            if (settings.notebookColorsEnabled)
                return preferences.colorForNotebook(root.note.notebookGuid)
            else
                return theme.palette.normal.foregroundText
        }

        title: note.title
        style: PageHeaderStyle {
            foregroundColor: header.notebookColor
        }
        trailingActionBar.actions: [
            Action {
                text: i18n.tr("Set")
                iconName: "ok"

                onTriggered: {
                    var date = datePicker.date
                    var time = timePicker.date
                    date.setHours(time.getHours())
                    date.setMinutes(time.getMinutes())

                    NotesStore.note(root.note.guid).reminder = true
                    NotesStore.note(root.note.guid).reminderTime = date

                    console.log("reminder set to " + Qt.formatDateTime(NotesStore.note(root.note.guid).reminderTime, Qt.LocalDate))

                    NotesStore.saveNote(root.note.guid)
                    apl.removePages(root)
                }
            }
        ]
        extension: Rectangle {
            width: header.width
            height: units.gu(0.5)
            color: header.notebookColor
        }
    }

    Flickable {
        id: timeDatePickerFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors {
            top: root.header.bottom
            bottom: root.bottom
            left: root.left
            right: root.right
            topMargin: units.gu(2)
        }

        contentHeight: pickerColumn.childrenRect.height

        Column {
            id: pickerColumn
            spacing: units.gu(2)
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Select date")
            }

            DatePicker {
                id: datePicker
                anchors.horizontalCenter: parent.horizontalCenter

                // DatePicker doesn't show the correct date/time without this trick
                Component.onCompleted: {
                    dateDelayTimer.start()
                }
            }

            Timer {
                id: dateDelayTimer
                interval: 500

                onTriggered: {
                    datePicker.date = root.note.reminder ? root.note.reminderTime : new Date()
                }
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Select time")
            }

            DatePicker {
                id: timePicker
                anchors.horizontalCenter: parent.horizontalCenter
                mode: "Hours|Minutes"

                // DatePicker doesn't show the correct date/time without this trick
                Component.onCompleted: {
                    timeDelayTimer.start()
                }
            }

            Timer {
                id: timeDelayTimer
                interval: 500

                onTriggered: {
                    timePicker.date = root.note.reminder ? root.note.reminderTime : new Date()
                }
            }

            Button {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Delete reminder")
                color: theme.palette.normal.negative
                visible: root.note.reminder

                onClicked: {
                    var popup = PopupUtils.open(Qt.resolvedUrl("../components/DeleteConfirmationDialog.qml"),
                                                root,
                                                { targetType: i18n.tr("reminder"), targetTitle: Qt.formatDateTime(root.note.reminderTime, Qt.LocalDate) })
                    popup.confirmed.connect(function() {
                        NotesStore.note(root.note.guid).reminder = false
                        NotesStore.note(root.note.guid).hasReminderTime = false
                        NotesStore.saveNote(root.note.guid)
                        apl.removePages(root)
                    })
                }
            }
        }
    }
}
