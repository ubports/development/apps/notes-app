/*
 * Copyright: 2019 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem

ListItem.Empty {
    id: root
    height: content.height

    SlotsLayout {
        id: content

        function themeIndex(themeName) {
            for (var i = 0; i < themeSelector.model.length; ++i) {
                if(themeName == themeSelector.model[i])
                {
                    return i
                }
            }
        }

        mainSlot: OptionSelector {
            id: themeSelector
            text: i18n.tr("App theme")
            containerHeight: itemHeight * themeSelector.model.length
            model: [
                i18n.tr("System"),
                i18n.tr("Ambiance"),
                i18n.tr("SuruDark")
            ]

            onSelectedIndexChanged: {
                settings.appTheme = themeSelector.model[themeSelector.selectedIndex]
                setCurrentTheme()
            }

            Component.onCompleted: {
                themeSelector.selectedIndex = content.themeIndex(settings.appTheme)
            }
        }
    }
}
