/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3

Page {
    id: root

    header: PageHeader {
        id: header

        contents: TextField {
            id: searchField
            anchors.fill: parent
            placeholderText: i18n.tr("Search tags...")
            inputMethodHints: Qt.ImhNoPredictiveText

            onTextChanged: {
                tags.searchText = searchField.text
            }
        }
    }

    TagsPage {
        id: tags
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        page: root
    }

    Timer {
        id: focusDelayTimer
        interval: 100

        onTriggered: {
            searchField.focus = true
        }
    }

    Component.onCompleted: {
        focusDelayTimer.start()
    }
}
