/*
 * Copyright: 2013 - 2014 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Lomiri.Components.Popups 1.3
import Evernote 0.1

import "../components"

Item {
    id: root
    objectName: "tagsPage"

    property var page
    property string searchText: ""

    function containsSearchText(name) {
        return name.toLowerCase().includes(root.searchText.toLowerCase())
    }

    Tags {
        id: tags
    }

    LomiriListView {
        id: tagsListView
        anchors.fill: parent
        pullToRefresh.enabled: preferences.accountName != "@local"
        pullToRefresh.refreshing: tags.loading
        pullToRefresh.onRefresh: tags.refresh()
        currentIndex: -1 // Workaround: LomiriListView seems to auto-highlight the 1st item
        clip: true
        model: tags
        delegate: TagDelegate {
            height: root.containsSearchText(model.name) ? units.gu(8) : units.gu(0)
            tag: model

            onOpenTag: {
                apl.addPageToCurrentColumn(root.page, Qt.resolvedUrl("FilteredNotesPage.qml"),
                                           { title: model.name, filterTagGuid: model.guid })
            }
            onDeleteTag: {
                var confirmation = PopupUtils.open(Qt.resolvedUrl("../components/DeleteConfirmationDialog.qml"),
                                                   root,
                                                   { targetType: i18n.tr("tag"), targetTitle: model.name })
                confirmation.confirmed.connect(function() { NotesStore.expungeTag(model.guid) })
            }
            onRenameTag: {
                var popup = PopupUtils.open(Qt.resolvedUrl("../components/RenameDialog.qml"), root,
                                            { targetType: i18n.tr("tag"), targetName: model.name })

                popup.confirmed.connect(function(newName) {
                    tags.tag(index).name = newName
                    NotesStore.saveTag(model.guid)
                })
            }
        }
    }

    Label {
        anchors.centerIn: parent
        visible: !tags.loading && tagsListView.count == 0
        width: parent.width - units.gu(4)
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        text: i18n.tr("No tags available")
    }
}
