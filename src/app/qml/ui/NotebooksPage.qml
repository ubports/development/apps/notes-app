/*
 * Copyright: 2013 - 2014 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Lomiri.Components.Popups 1.3
import Evernote 0.1

import "../components"

Item {
    id: root
    objectName: "notebooksPage"

    property var page
    property string searchText: ""

    function containsSearchText(name) {
        return name.toLowerCase().includes(root.searchText.toLowerCase())
    }

    Notebooks {
        id: notebooks
    }

    LomiriListView {
        id: notebooksListView
        anchors.fill: parent
        pullToRefresh.enabled: preferences.accountName != "@local"
        pullToRefresh.refreshing: notebooks.loading
        pullToRefresh.onRefresh: notebooks.refresh()
        currentIndex: -1 // Workaround: LomiriListView seems to auto-highlight the 1st item
        clip: true
        model: notebooks
        delegate: NotebookDelegate {
            height: root.containsSearchText(model.name) ? units.gu(8) : units.gu(0)
            notebook: model

            onOpenNotebook: {
                apl.addPageToCurrentColumn(root.page, Qt.resolvedUrl("FilteredNotesPage.qml"),
                                           { title: model.name, filterNotebookGuid: model.guid })
            }
            onDeleteNotebook: {
                var popup = PopupUtils.open(Qt.resolvedUrl("../components/DeleteConfirmationDialog.qml"), root,
                                            { targetType: i18n.tr("notebook"), targetTitle: model.name })

                popup.confirmed.connect(function() {
                    NotesStore.expungeNotebook(model.guid)
                })
            }
            onRenameNotebook: {
                var popup = PopupUtils.open(Qt.resolvedUrl("../components/RenameDialog.qml"), root,
                                            { targetType: i18n.tr("notebook"), targetName: model.name })

                popup.confirmed.connect(function(newName) {
                    notebooks.notebook(index).name = newName
                    NotesStore.saveNotebook(model.guid)
                })
            }
            onSetAsDefault: {
                NotesStore.setDefaultNotebook(model.guid)
            }
        }
    }
}
