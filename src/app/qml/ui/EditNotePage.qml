/*
 * Copyright: 2013 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Lomiri.Components.Themes.Ambiance 1.3
import Evernote 0.1

import "../components"

Page {
    id: root

    property var note

    function saveNote() {
        Qt.inputMethod.commit()

        var title = titleField.text ? titleField.text : i18n.tr("Untitled")
        var text = noteTextArea.text

        if (root.note) {
            root.note.title = title
            root.note.richTextContent = text

            NotesStore.saveNote(root.note.guid)
        }
        else {
            root.note = NotesStore.createNote(title, "", text)
        }
    }

    header: PageHeader {
        id: header

        property string notebookColor: {
            if (settings.notebookColorsEnabled && root.note)
                return preferences.colorForNotebook(root.note.notebookGuid)
            else
                return theme.palette.normal.foregroundText
        }

        contents: TextField {
            id: titleField
            anchors.fill: parent
            text: root.note ? root.note.title : i18n.tr("Untitled")
            font.weight: Font.DemiBold
        }
        style: PageHeaderStyle {
            foregroundColor: header.notebookColor
        }
        trailingActionBar.numberOfSlots: 3
        trailingActionBar.actions: [
            Action {
                text: i18n.tr("Save and close")
                iconName: "ok"

                onTriggered: {
                    root.saveNote()
                    apl.removePages(root)
                }
            },
            Action {
                text: i18n.tr("Filters")
                iconName: "filters"

                onTriggered: {
                    root.saveNote()
                    apl.addPageToCurrentColumn(root, Qt.resolvedUrl("EditNoteFiltersPage.qml"), { note: root.note })
                }
            }
        ]
        extension: Rectangle {
            width: header.width
            height: units.gu(0.5)
            color: header.notebookColor
        }
    }

    TextArea {
        id: noteTextArea
        objectName: "noteTextArea"
        anchors.top: header.bottom
        anchors.bottom: toolbox.top
        anchors.left: parent.left
        anchors.right: parent.right
        wrapMode: TextEdit.Wrap
        textFormat: TextEdit.RichText
        text: root.note ? root.note.richTextContent : ""
    }

    ToolBox {
        id: toolbox
        anchors.bottom: keyboard.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottomMargin: units.gu(1)
        page: root
    }

    Connections {
        target: Qt.application
        onActiveChanged: {
            root.saveNote()
        }
    }

    Item {
        id: keyboard
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: Qt.inputMethod.keyboardRectangle.height
    }

    Timer {
        id: focusDelayTimer
        interval: 100

        onTriggered: {
            noteTextArea.focus = true
            noteTextArea.cursorPosition = noteTextArea.length
        }
    }

    Component.onCompleted: {
        focusDelayTimer.start()
    }

    Component.onDestruction: {
        root.saveNote()
    }
}

