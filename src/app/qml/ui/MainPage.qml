/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Evernote 0.1

import "../components"

Page {
    id: root

    property int selectedIndex: 0

    header: PageHeader {
        contents: Row {
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height
            spacing: units.gu(1)

            ActionIcon {
                id: noteActionIcon
                iconName: "note"
                index: 0
                selectedIndex: root.selectedIndex

                onSelected: {
                    root.selectedIndex = noteActionIcon.index
                    pageBodyLoader.setSource(Qt.resolvedUrl("NotesPage.qml"), { page : root })
                    bottomEdge.visible = true
                    bottomEdge.text = i18n.tr("Add note")
                }
            }

            ActionIcon {
                id: notebookActionIcon
                iconName: "notebook"
                index: 1
                selectedIndex: root.selectedIndex

                onSelected: {
                    root.selectedIndex = notebookActionIcon.index
                    pageBodyLoader.setSource(Qt.resolvedUrl("NotebooksPage.qml"), { page : root })
                    bottomEdge.visible = true
                    bottomEdge.text = i18n.tr("Add notebook")
                }
            }

            ActionIcon {
                id: tagActionIcon
                iconName: "tag"
                index: 2
                selectedIndex: root.selectedIndex

                onSelected: {
                    root.selectedIndex = tagActionIcon.index
                    pageBodyLoader.setSource(Qt.resolvedUrl("TagsPage.qml"), { page : root })
                    bottomEdge.visible = true
                    bottomEdge.text = i18n.tr("Add tag")
                }
            }

            ActionIcon {
                id: reminderActionIcon
                iconName: "reminder"
                index: 3
                selectedIndex: root.selectedIndex

                onSelected: {
                    root.selectedIndex = reminderActionIcon.index
                    pageBodyLoader.setSource(Qt.resolvedUrl("RemindersPage.qml"), { page : root })
                    bottomEdge.visible = false
                }
            }
        }
        trailingActionBar.numberOfSlots: 3
        trailingActionBar.actions: [
            Action {
                text: i18n.tr("Settings")
                iconName: "settings"

                onTriggered: {
                    apl.addPageToCurrentColumn(root, Qt.resolvedUrl("SettingsPage.qml"))
                }
            },
            Action {
                text: i18n.tr("Search")
                iconName: "search"

                onTriggered: {
                    switch(root.selectedIndex) {
                        case notebookActionIcon.index:
                            apl.addPageToCurrentColumn(root, Qt.resolvedUrl("SearchNotebooksPage.qml"))
                            break
                        case tagActionIcon.index:
                            apl.addPageToCurrentColumn(root, Qt.resolvedUrl("SearchTagsPage.qml"))
                            break
                        default:
                            apl.addPageToCurrentColumn(root, Qt.resolvedUrl("SearchNotesPage.qml"))
                            break
                    }
                }
            }
        ]
    }

    Loader {
        id: pageBodyLoader
        anchors.top: root.header.bottom
        anchors.bottom: bottomEdge.top
        anchors.left: root.left
        anchors.right: root.right
    }

    BottomEdgeButton {
        id: bottomEdge
        anchors.bottom: root.bottom
        anchors.left: root.left
        anchors.right: root.right

        onClicked: {
            switch(root.selectedIndex) {
                case noteActionIcon.index:
                    NotesStore.createNote(i18n.tr("Untitled"))
                    break
                case notebookActionIcon.index:
                    var popup = PopupUtils.open(Qt.resolvedUrl("../components/RenameDialog.qml"), root, { targetType: i18n.tr("notebook") })

                    popup.confirmed.connect(function(name) {
                        NotesStore.createNotebook(name)
                    })
                    break
                case tagActionIcon.index:
                    var popup = PopupUtils.open(Qt.resolvedUrl("../components/RenameDialog.qml"), root, { targetType: i18n.tr("tag") })

                    popup.confirmed.connect(function(name) {
                        NotesStore.createTag(name)
                    })
                    break
                default:
                    print(root.selectedIndex + " does not support the bottom action")
                    break
            }
        }
    }

    Component.onCompleted: {
        noteActionIcon.selected()
    }
}
