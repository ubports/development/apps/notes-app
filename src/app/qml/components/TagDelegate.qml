/*
 * Copyright: 2013 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3

ListItem {
    id: root
    // Workaround: LomiriListView doesn't add dividers to dynamically added ListItems
    // We are implementing our own divider
    divider.visible: false

    property var tag

    signal openTag()
    signal deleteTag()
    signal renameTag()

    leadingActions: ListItemActions {
        actions: [
            Action {
                text: i18n.tr("Delete")
                iconName: "delete"

                onTriggered: root.deleteTag()
            }
        ]
    }
    action: Action {
        onTriggered: root.openTag()
    }
    trailingActions: ListItemActions {
        actions: [
            Action {
                text: i18n.tr("Edit")
                iconName: "edit"

                onTriggered: root.renameTag()
            }
        ]
    }

    Column {
        anchors.fill: parent

        Item {
            width: parent.width
            height: parent.height - tagDivider.height

            Row {
                anchors.fill: parent
                anchors.margins: units.gu(1)

                Label {
                    width: parent.width - tagStats.width
                    height: parent.height
                    text: root.tag.name
                    verticalAlignment: Text.AlignVCenter
                    font.strikeout: root.tag.deleted
                }

                Item {
                    id: tagStats
                    width: units.gu(2)
                    height: parent.height

                    Column {
                        anchors.fill: parent

                        Label {
                            width: parent.width
                            height: width
                            text: "(" + root.tag.noteCount + ")"
                            textSize: Label.XSmall
                            horizontalAlignment: Text.AlignHCenter
                        }
                        Icon {
                            width: parent.width
                            height: width
                            source: "../images/sync-none.svg"
                            visible: preferences.accountName == "@local"
                        }
                        Icon {
                            width: parent.width
                            height: width
                            name: root.tag.loading ? "sync-updating" :
                                  root.tag.syncError ? "sync-error" :
                                  root.tag.synced ? "sync-idle" : "sync-offline"
                            visible: preferences.accountName != "@local"
                        }
                    }
                }
            }
        }

        Rectangle {
            id: tagDivider
            width: parent.width
            height: units.gu(0.1)
            color: theme.palette.normal.base
        }
    }
}
