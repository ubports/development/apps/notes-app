/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3

Rectangle {
    id: root
    height: units.gu(4)
    color: theme.palette.normal.foreground

    property alias text: _label.text

    signal clicked()

    AbstractButton {
        anchors.fill: parent

        onClicked: root.clicked()

        Row {
            anchors.fill: parent
            anchors.topMargin: (parent.height - _icon.height) / 2
            anchors.leftMargin: (parent.width - _icon.width - _label.width - spacing) / 2
            spacing: units.gu(1)

            Icon {
                id: _icon
                width: _label.height
                height: width
                name: "add"
                color: theme.palette.normal.foregroundText
            }

            Label {
                id: _label
                textSize: Label.Medium
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }
    }
}
