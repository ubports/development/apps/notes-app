/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: root
    title: i18n.tr("Details")

    property var note

    ListItem {
        height: _created.height

        ListItemLayout {
            id: _created
            title.text: i18n.tr("Created")
            subtitle.text: Qt.formatDateTime(note.created, Qt.LocalDate)
        }
    }

    ListItem {
        height: _updated.height
        divider.visible: false

        ListItemLayout {
            id: _updated
            title.text: i18n.tr("Updated")
            subtitle.text: Qt.formatDateTime(note.updated, Qt.LocalDate)
        }
    }

    Button {
        text: i18n.tr("Close")

        onClicked: {
            PopupUtils.close(root)
        }
    }
}
