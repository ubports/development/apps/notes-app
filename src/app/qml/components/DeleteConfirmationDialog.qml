/*
 * Copyright: 2020 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: root

    property string targetType // note, notebook, tag or reminder
    property string targetTitle

    signal confirmed()

    title: i18n.tr("Delete")
    // TRANSLATORS: %1 is translatable, it is one of the following: note, notebook, tag, reminder.
    //              %2 is untranslatable, it is the name of the note, notebook, tag or reminder.
    text: i18n.tr("Are you sure you want to delete %1 <b>%2</b>?").arg(targetType).arg(targetTitle)

    Button {
        text: i18n.tr("Delete")
        color: theme.palette.normal.negative

        onClicked: {
            root.confirmed()
            PopupUtils.close(root)
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: {
            PopupUtils.close(root)
        }
    }
}
