/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: root

    property string targetType // notebook or tag
    property string targetName: ""
    property string actionName: (root.targetName.length > 0) ? i18n.tr("Rename") : i18n.tr("Create")

    signal confirmed(string newName)

    title: actionName + " " + targetType

    TextField {
        id: nameTextField
        text: root.targetName
        placeholderText: i18n.tr("Name cannot be empty")
    }

    Button {
        text: actionName
        color: theme.palette.normal.positive
        enabled: nameTextField.displayText.trim()

        onClicked: {
            root.confirmed(nameTextField.displayText.trim())
            PopupUtils.close(root)
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: {
            PopupUtils.close(root)
        }
    }

    Component.onCompleted: {
        nameTextField.focus = true
        nameTextField.cursorPosition = root.targetName.length
    }
}
