/*
 * Copyright: 2020 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Brian Douglass
 */

#include "notehelper.h"

#include <QStandardPaths>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <QCoreApplication>
#include <QTextStream>

NoteHelper *NoteHelper::s_instance = 0;

NoteHelper::NoteHelper(QObject *parent):
    QObject(parent)
{
    // Randomly pick a port every time the app is launched
    m_port = qrand() % (65535 - 10000) + 10000;
}

NoteHelper *NoteHelper::instance()
{
    if (!s_instance)
    {
        s_instance = new NoteHelper();
    }
    return s_instance;
}

int NoteHelper::port() const
{
    return m_port;
}

QString NoteHelper::notePath() const
{
    QString cacheDir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    return cacheDir + QDir::separator() + QStringLiteral("index.html");
}

void NoteHelper::writeNote(QString html, QString backgroundColor, QString textColor) const
{
    QString cacheDir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    if (!QDir(cacheDir).exists())
    {
        QDir().mkdir(cacheDir);
    }

    // TODO see if there is a better way to do this
    QString injectFilePath = QCoreApplication::applicationDirPath() + "/../Evernote/inject.html";
    qDebug() << injectFilePath;
    QFile injectFile(injectFilePath);

    injectFile.open(QFile::ReadOnly | QFile::Text);
    QTextStream in(&injectFile);

    int index = html.indexOf(QStringLiteral("</head>"));
    QString modifiedHtml = html.insert(index, in.readAll())
        .replace("PORT", QString::number(m_port))
        .replace("BACKGROUND_COLOR", backgroundColor)
        .replace("TEXT_COLOR", textColor);

    QFile file(notePath());
    file.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
    file.write(modifiedHtml.toUtf8());
    file.close();
}
